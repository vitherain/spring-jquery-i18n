<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Spring MVC and jQuery i18n using common *.properties</title>
	</head>
	<body>
		<a href="?lang=cs">Czech</a> &times; <a href="?lang=en">English</a>
		<br />
		<spring:message code="label.spring" text="Default text - spring" />
		
		<div id="jquery-text"></div>
		
		<script	src="${pageContext.request.contextPath}/resources/lib/jquery-1.11.3/jquery.min.js" ></script>
		<script	src="${pageContext.request.contextPath}/resources/lib/jquery-i18n-1.0.9/jquery.i18n.properties-min-1.0.9.js"></script>
		<script>
			var locale = "${pageContext.response.locale.language}";
		</script>
		<script	src="${pageContext.request.contextPath}/resources/js/i18n-configuration.js"></script>
		<script>
			$("#jquery-text").text($.i18n.prop("label.jquery"));
		</script>
	</body>
</html>